#!/usr/bin/python

import keras
import tensorflow as tf
import sys
import os
from os.path import join
import numpy as np
import itertools
import random
import json
from scipy import ndimage
from keras import backend as K
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Input, Dense, Activation
from keras.layers import Reshape, Lambda
from keras.layers.merge import add, concatenate
from keras.models import Model, load_model
from keras.layers.recurrent import GRU
from keras.optimizers import SGD
from keras.utils.data_utils import get_file
from keras.preprocessing import image
import keras.callbacks
import cv2
import argparse

import http.server
import urllib.parse
from http.server import BaseHTTPRequestHandler, HTTPServer
import cgi
import json

letters = list('0123456789ABCEHKMOPTXY')

model = None
net_inp = None
net_out = None

sess = tf.Session()
K.set_session(sess)

def text_to_labels(text):
    return list(map(lambda x: letters.index(x), text))

def is_valid_str(s):
    for ch in s:
        if not ch in letters:
            return False
    return True


class ImageGenerator:

    def __init__(self,
                 imgPath,
                 img_w, img_h,
                 downsample_factor,
                 max_text_len=8):

        self.img_h = img_h
        self.img_w = img_w
        self.imgPath = imgPath
        self.max_text_len = max_text_len
        self.downsample_factor = downsample_factor

    def build_data(self):
        print ('Reading image...')
        
        self.img = np.zeros((self.img_h, self.img_w))
        image = cv2.imread(self.imgPath)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.resize(image, (self.img_w, self.img_h))
        image = image.astype(np.float32)
        image /= 255
        self.img[:, :] = image
    
        if image is None:
            print ('Error reading image...')

    def next_batch(self):
        while True:
            # width and height are backwards from typical Keras convention
            # because width is the time dimension when it gets fed into the RNN
            if K.image_data_format() == 'channels_first':
                X_data = np.ones([1, 1, self.img_w, self.img_h])
            else:
                X_data = np.ones([1, self.img_w, self.img_h, 1])
            img = self.img
            img = img.T
            
            if K.image_data_format() == 'channels_first':
                img = np.expand_dims(img, 0)
            else:
                img = np.expand_dims(img, -1)
            X_data[0] = img

            print ('DATA:', X_data)
      
            inputs = {
                'the_input': X_data,
                #'source_str': source_str
            }
            yield (inputs)

def decode_batch(out):
    ret = []
    for j in range(out.shape[0]):
        out_best_values = list(np.amax(out[j, 2:], 1))
        out_best_indexes = list(np.argmax(out[j, 2:], 1))

        confidenceSum = 0
        confidenceCount = 0
        out_best = [] 
        for i in range(len(out_best_indexes)):
            currentLetterIndex = out_best_indexes[i]
            currentValue = out_best_values[i]
            
            confidenceCount += 1
            confidenceSum += currentValue

            if i+1 < len(out_best_indexes):
                if currentLetterIndex == out_best_indexes[i+1]:
                    continue    

            out_best.append((currentLetterIndex, confidenceSum/confidenceCount))
            confidenceCount = 0
            confidenceSum = 0

        outstr = ''
        valuable_confidence = []
        for (li, conf) in out_best:
            if li < len(letters):
                outstr += letters[li]
                valuable_confidence.append(conf)
        ret.append((outstr , np.median(valuable_confidence)))

    return ret


class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
    
    def parse_POST(self):
        ctype, pdict = cgi.parse_header(self.headers['content-type'])
        if ctype == 'multipart/form-data':
            postvars = parse_multipart(self.rfile, pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers['content-length'])
            postvars = urllib.parse.parse_qs(
                                self.rfile.read(length),
                                keep_blank_values=1)
        else:
            postvars = {}
        
        return postvars
    
    def do_POST(self):
        postvars = self.parse_POST()
        for i in postvars:
            print (i, postvars[i])
        
        imgPathParam = postvars.get(bytes("img_path", "utf8"), None)
        
        plate = ''
        confidence = 0
        
        if not (imgPathParam is None):
            imgAbsPath = imgPathParam[0].decode("utf-8")
            
            print ('Image path: ', imgAbsPath)
            
            if os.path.isfile(imgAbsPath):
                print ('Image exists')
                
                tiger_test = ImageGenerator(imgAbsPath, 128, 64, 4)
                tiger_test.build_data()

                for inp_value in tiger_test.next_batch():
                    print ('Processing first input...')
                    
                    if inp_value is None:
                        print ('Empty inp_value')
                    
                    bs = inp_value['the_input'].shape[0]
                   
                    if bs is None:
                        print ('Empty shape')
                    
                    X_data = inp_value['the_input']
                    
                    if X_data is None:
                        print ('Empty X_data')
                    
                    print ('X_data: ', type(X_data))
                    
                    
                    net_out_value = sess.run(net_out, feed_dict={net_inp:X_data})
                    predictions = decode_batch(net_out_value)
                    for i in range(bs):
                        plate, confidence = predictions[i]
                        break
                    break
            else:
                print ('Can\'t load image !')
        else:
            print ('Wrong img_path parameter !' )
        
        
        jsonStr = json.dumps({"results": [{"plate": "%s" % (plate), "confidence": confidence }]})
        
        self.wfile.write(bytes(jsonStr, "utf8"))
    
    def do_HEAD(self):
        self._set_headers()

def run(server_class=HTTPServer, handler_class=S, port=2323, modelPath='data/model_36.h5'):
    global model
    global net_inp
    global net_out


    modelAbsPath = os.path.abspath(modelPath)
    model = load_model(modelAbsPath, compile=False)
    
    net_inp = model.get_layer(name='the_input').input
    net_out = model.get_layer(name='softmax').output
    
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print ('Starting tensor http:', port)
    httpd.serve_forever()

parser = argparse.ArgumentParser()
parser.add_argument("model")
args = parser.parse_args()

run(modelPath = args.model)
