/*
 * Copyright 2018 The TensorFlow Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tensorflow.demo;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.ImageReader.OnImageAvailableListener;
import android.media.MediaMetadataRetriever;
import android.os.SystemClock;
import android.util.Size;
import android.util.TypedValue;
import android.widget.Toast;

import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.tensorflow.demo.OverlayView.DrawCallback;
import org.tensorflow.demo.env.BorderedText;
import org.tensorflow.demo.env.ImageUtils;
import org.tensorflow.demo.env.Logger;
import org.tensorflow.demo.tracking.MultiBoxTracker;
import org.tensorflow.lite.demo.R; // Explicit import needed for internal Google builds.

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.Image.Plane;
import android.media.ImageReader;
import android.media.ImageReader.OnImageAvailableListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Trace;
import android.util.Size;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.Toast;
import java.nio.ByteBuffer;
import org.tensorflow.demo.env.ImageUtils;
import org.tensorflow.demo.env.Logger;

/**
 * An activity that uses a TensorFlowMultiBoxDetector and ObjectTracker to detect and then track
 * objects.
 */
public class DetectorActivity extends CameraActivity implements OnImageAvailableListener {
    private static final Logger LOGGER = new Logger();

    // Configuration values for the prepackaged SSD model.
    private static final int TF_OD_API_INPUT_SIZE = 300;
    private static final boolean TF_OD_API_IS_QUANTIZED = true;
    private static final String TF_OD_API_MODEL_FILE = "linza_detect.tflite";
    private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/plate_labels_list.txt";

    // Which detection model to use: by default uses Tensorflow Object Detection API frozen
    // checkpoints.
    private enum DetectorMode {
        TF_OD_API;
    }

    private static final DetectorMode MODE = DetectorMode.TF_OD_API;

    // Minimum detection confidence to track a detection.
    private static final float MINIMUM_CONFIDENCE_TF_OD_API = 0.6f;

    private static final boolean MAINTAIN_ASPECT = false;

    private static final Size DESIRED_PREVIEW_SIZE = new Size(640, 480);

    private static final boolean SAVE_PREVIEW_BITMAP = false;
    private static final float TEXT_SIZE_DIP = 10;

    private Integer sensorOrientation;

    private Classifier detector;

    private long lastProcessingTimeMs;
    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;
    private Bitmap cropCopyBitmap = null;

    private boolean computingDetection = false;

    private long timestamp = 0;

    private Matrix frameToCropTransform;
    private Matrix cropToFrameTransform;

    private MultiBoxTracker tracker;

    private byte[] luminanceCopy;

    private BorderedText borderedText;

    protected int cropSize;

    String uri = "/storage/sdcard0/all_new.mp4";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        LOGGER.d("onCreate " + this);
        super.onCreate(null);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

//    setContentView(R.layout.activity_camera);

//    if (hasPermission()) {
//      setFragment();
//    } else {
//      requestPermission();
//    }

        runRecognizer();
    }


    protected void runRecognizer() {


        try {
            detector =
                    TFLiteObjectDetectionAPIModel.create(
                            getAssets(),
                            TF_OD_API_MODEL_FILE,
                            TF_OD_API_LABELS_FILE,
                            TF_OD_API_INPUT_SIZE,
                            TF_OD_API_IS_QUANTIZED);
            cropSize = TF_OD_API_INPUT_SIZE;
        } catch (final IOException e) {
            LOGGER.e("Exception initializing classifier!", e);
            Toast toast =
                    Toast.makeText(
                            getApplicationContext(), "Classifier could not be initialized", Toast.LENGTH_SHORT);
            toast.show();
            finish();
        }

        previewWidth = 1920;
        previewHeight = 1080;

        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
        croppedBitmap = Bitmap.createBitmap(cropSize, cropSize, Config.ARGB_8888);

        frameToCropTransform =
                ImageUtils.getTransformationMatrix(
                        previewWidth, previewHeight,
                        cropSize, cropSize,
                        0, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);

        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(uri);
        int duration = 1221000000;
        int m = 500000;
        for (int i = 0; i < duration; i += m) {
            LOGGER.i("location recognized processCurrentBitmap at frame with completed percentage %f", ((float) i) / duration);
            rgbFrameBitmap = mediaMetadataRetriever.getFrameAtTime(i);
            processCurrentBitmap();
        }
    }

    void processCurrentBitmap() {
//    rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);

//    if (luminanceCopy == null) {
//      luminanceCopy = new byte[originalLuminance.length];
//    }
//    System.arraycopy(originalLuminance, 0, luminanceCopy, 0, originalLuminance.length);
//    readyForNextImage();

        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);

        final List<Classifier.Recognition> results = detector.recognizeImage(croppedBitmap);

        float minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
        switch (MODE) {
            case TF_OD_API:
                minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
                break;
        }

        final List<Classifier.Recognition> mappedRecognitions =
                new LinkedList<Classifier.Recognition>();

        for (final Classifier.Recognition result : results) {
            final RectF location = result.getLocation();
            if (location != null && result.getConfidence() >= minimumConfidence) {

                cropToFrameTransform.mapRect(location);
                result.setLocation(location);
                mappedRecognitions.add(result);

                try {
                    Bitmap jpgBitmap = Bitmap.createBitmap(rgbFrameBitmap, (int)location.left, (int)location.top, (int)location.right - (int)location.left, (int)location.bottom - (int)location.top);
                    Long tsLong = System.currentTimeMillis()/100;
                    jpgBitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(new File("/storage/sdcard0/recognized_plates/"+ tsLong + ".jpg")));
                } catch (Exception e) {
                    LOGGER.i("location recognized exception" + e.getMessage());
                }

                LOGGER.i("location recognized top %f left %f right %f bottom %f", location.top, location.left, location.right, location.bottom);
            }
        }

        //tracker.trackResults(mappedRecognitions, luminanceCopy, currTimestamp);
        //trackingOverlay.postInvalidate();

        //requestRender();
        //computingDetection = false;


    }
    @Override
    public void onPreviewSizeChosen(final Size size, final int rotation) {
    }

//    @Override
//    public void onPreviewSizeChosen(final Size size, final int rotation) {
//        final float textSizePx =
//                TypedValue.applyDimension(
//                        TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
//        borderedText = new BorderedText(textSizePx);
//        borderedText.setTypeface(Typeface.MONOSPACE);
//
//        tracker = new MultiBoxTracker(this);
//
//        int cropSize = TF_OD_API_INPUT_SIZE;
//
//        try {
//            detector =
//                    TFLiteObjectDetectionAPIModel.create(
//                            getAssets(),
//                            TF_OD_API_MODEL_FILE,
//                            TF_OD_API_LABELS_FILE,
//                            TF_OD_API_INPUT_SIZE,
//                            TF_OD_API_IS_QUANTIZED);
//            cropSize = TF_OD_API_INPUT_SIZE;
//        } catch (final IOException e) {
//            LOGGER.e("Exception initializing classifier!", e);
//            Toast toast =
//                    Toast.makeText(
//                            getApplicationContext(), "Classifier could not be initialized", Toast.LENGTH_SHORT);
//            toast.show();
//            finish();
//        }
//
//
//        previewWidth = size.getWidth();
//        previewHeight = size.getHeight();
//
//        sensorOrientation = rotation - getScreenOrientation();
//        LOGGER.i("Camera orientation relative to screen canvas: %d", sensorOrientation);
//
//        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
//        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Config.ARGB_8888);
//        croppedBitmap = Bitmap.createBitmap(cropSize, cropSize, Config.ARGB_8888);
//
//        frameToCropTransform =
//                ImageUtils.getTransformationMatrix(
//                        previewWidth, previewHeight,
//                        cropSize, cropSize,
//                        sensorOrientation, MAINTAIN_ASPECT);
//
//        cropToFrameTransform = new Matrix();
//        frameToCropTransform.invert(cropToFrameTransform);
//
//        trackingOverlay = (OverlayView) findViewById(R.id.tracking_overlay);
//        trackingOverlay.addCallback(
//                new DrawCallback() {
//                    @Override
//                    public void drawCallback(final Canvas canvas) {
//                        tracker.draw(canvas);
//                        if (isDebug()) {
//                            tracker.drawDebug(canvas);
//                        }
//                    }
//                });
//
//        addCallback(
//                new DrawCallback() {
//                    @Override
//                    public void drawCallback(final Canvas canvas) {
//                        if (!isDebug()) {
//                            return;
//                        }
//                        final Bitmap copy = cropCopyBitmap;
//                        if (copy == null) {
//                            return;
//                        }
//
//                        final int backgroundColor = Color.argb(100, 0, 0, 0);
//                        canvas.drawColor(backgroundColor);
//
//                        final Matrix matrix = new Matrix();
//                        final float scaleFactor = 2;
//                        matrix.postScale(scaleFactor, scaleFactor);
//                        matrix.postTranslate(
//                                canvas.getWidth() - copy.getWidth() * scaleFactor,
//                                canvas.getHeight() - copy.getHeight() * scaleFactor);
//                        canvas.drawBitmap(copy, matrix, new Paint());
//
//                        final Vector<String> lines = new Vector<String>();
//                        if (detector != null) {
//                            final String statString = detector.getStatString();
//                            final String[] statLines = statString.split("\n");
//                            for (final String line : statLines) {
//                                lines.add(line);
//                            }
//                        }
//                        lines.add("");
//
//                        lines.add("Frame: " + previewWidth + "x" + previewHeight);
//                        lines.add("Crop: " + copy.getWidth() + "x" + copy.getHeight());
//                        lines.add("View: " + canvas.getWidth() + "x" + canvas.getHeight());
//                        lines.add("Rotation: " + sensorOrientation);
//                        lines.add("Inference time: " + lastProcessingTimeMs + "ms");
//
//                        borderedText.drawLines(canvas, 10, canvas.getHeight() - 10, lines);
//                    }
//                });
//    }

    OverlayView trackingOverlay;

    @Override
    protected void processImage() {

    }
//    @Override
//    protected void processImage() {
//        ++timestamp;
//        final long currTimestamp = timestamp;
//        byte[] originalLuminance = getLuminance();
//        tracker.onFrame(
//                previewWidth,
//                previewHeight,
//                getLuminanceStride(),
//                sensorOrientation,
//                originalLuminance,
//                timestamp);
//        trackingOverlay.postInvalidate();
//
//        // No mutex needed as this method is not reentrant.
//        if (computingDetection) {
//            readyForNextImage();
//            return;
//        }
//        computingDetection = true;
//        LOGGER.i("Preparing image " + currTimestamp + " for detection in bg thread.");
//
//        rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);
//
//        if (luminanceCopy == null) {
//            luminanceCopy = new byte[originalLuminance.length];
//        }
//        System.arraycopy(originalLuminance, 0, luminanceCopy, 0, originalLuminance.length);
//        readyForNextImage();
//
//        final Canvas canvas = new Canvas(croppedBitmap);
//        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);
//        // For examining the actual TF input.
//        if (SAVE_PREVIEW_BITMAP) {
//            ImageUtils.saveBitmap(croppedBitmap);
//        }
//
//        runInBackground(
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        LOGGER.i("Running detection on image " + currTimestamp);
//                        final long startTime = SystemClock.uptimeMillis();
//                        final List<Classifier.Recognition> results = detector.recognizeImage(croppedBitmap);
//                        lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;
//
////                        cropCopyBitmap = Bitmap.createBitmap(croppedBitmap);
////                        final Canvas canvas = new Canvas(cropCopyBitmap);
////                        final Paint paint = new Paint();
////                        paint.setColor(Color.RED);
////                        paint.setStyle(Style.STROKE);
////                        paint.setStrokeWidth(2.0f);
//
//                        float minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
//                        switch (MODE) {
//                            case TF_OD_API:
//                                minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
//                                break;
//                        }
//
//                        final List<Classifier.Recognition> mappedRecognitions =
//                                new LinkedList<Classifier.Recognition>();
//
//                        for (final Classifier.Recognition result : results) {
//                            final RectF location = result.getLocation();
//                            if (location != null && result.getConfidence() >= minimumConfidence) {
//
//                                cropToFrameTransform.mapRect(location);
//                                result.setLocation(location);
//                                mappedRecognitions.add(result);
//                            }
//                        }
//
//                        //requestRender();
//                        computingDetection = false;
//                    }
//                });
//    }

    @Override
    protected int getLayoutId() {
        return R.layout.camera_connection_fragment_tracking;
    }

    @Override
    protected Size getDesiredPreviewFrameSize() {
        return DESIRED_PREVIEW_SIZE;
    }

    @Override
    public void onSetDebug(final boolean debug) {
        detector.enableStatLogging(debug);
    }
}
