# TODO: update the readme with new parameters
# TODO: restoring a model without recreating it (use constants / op names in the code?)
# TODO: move all the training parameters inside the training parser
# TODO: switch to https://www.tensorflow.org/api_docs/python/tf/nn/dynamic_rnn instead of buckets

from __future__ import absolute_import

import sys

import tensorflow as tf

from model.model import Model
from util import dataset
from util.data_gen import DataGen
from util.export import Exporter

def main(args=None):

    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))

    model = Model(
        output_dir='./results',
        batch_size=65,
        initial_learning_rate=1.0,
        steps_per_checkpoint=100,
        model_dir='./aocr_model',
        target_embedding_size=10,
        attn_num_hidden=128,
        attn_num_layers=2,
        clip_gradients=True,
        max_gradient_norm=5.0,
        session=sess,
        load_model=True,
        gpu_id=0,
        use_gru=False,
        use_distance=True,
        max_image_width=160,
        max_image_height=60,
        max_prediction_length=9,
        channels=1,
    )

    try:
        with open('./e2.jpg', 'rb') as img_file:
            img_file_data = img_file.read()
    except IOError:
        print('Result: error while opening file %s.', filename)
        return
    text, probability = model.predict(img_file_data)
    print('Result: OK. %s %s', '{:.2f}'.format(probability), text)


if __name__ == "__main__":
    main()
