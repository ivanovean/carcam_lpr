# carcam_lpr

Usage:
'python keras_plate.py <path/to/model> <path/to/image.jpg-jpeg-png>'

Samle:
'python keras_plate.py data/model_36.h5 data/real_test/img/5ade2b196033b94475812047.jpeg'


# carcam_anpr

Usage:
'python anpr_plate.py <path/to/model_anpr_dir> <path/to/image.jpg-jpeg-png>'
Output:
(x,y,width,height) - координаты номера

Samle:
'python anpr_plate.py model_anpr/ fake_plate.png'
'python anpr_plate.py model_anpr/ real_plate.png'